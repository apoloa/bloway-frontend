import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ImagesService } from '../../services/images.service';
import { Subject } from 'rxjs/Subject';
import { WebSocketMessage } from '../../models/webSocketMessage';
import { Image } from '../../models/image';
import { RowId } from '../../models/rowId';

@Component({
    selector: 'push-image',
    templateUrl: './push-image.component.html',
    styleUrls: ['./push-image.component.css']
})

export class PushImageComponent {

    @Input() row: number;
    @Output() completed: EventEmitter<RowId> = new EventEmitter();

    public imagesUploader: Subject<WebSocketMessage> = this._imagesService.createImage();
    sizePushed: number = 0;
    imageSize: number = null;
    url: string = null;

    constructor(private _imagesService: ImagesService) {
    }

    getProcessValue():number {
        if (this.imageSize) {
            return this.sizePushed / this.imageSize * 100;
        }
        return 0;
    }

    setUrl(url: string): void {
        this.url = url;
        let rowId = new RowId(this.row, null, url, 0, 0, null);
        this.completed.next(rowId);
    }

    setProcessValue(value): void {
        this.sizePushed = value;
    }

    completedImageReader(image: Image): void {
        let id = '';
        let block = 600;
        this.sizePushed = 0;
        this.imageSize = image.body.length;
        this.imagesUploader.next(new WebSocketMessage(WebSocketMessage.GET_ID, ''));
        this.imagesUploader.subscribe(msg => {
            switch (msg.messageId) {
                case WebSocketMessage.SEND_ID_FOR_IMAGE:
                    id = msg.message;
                    this.imagesUploader.next(
                        new WebSocketMessage(WebSocketMessage.RECEIVE_IMAGE,
                            { id: id, type: image.extension, body: image.body.substr(0, block) }));
                    break;
                case WebSocketMessage.SEND_CORRECT_READ:
                    let response = JSON.parse(msg.message);
                    this.setProcessValue(this.sizePushed + response.bytesRead);
                    if (image.body.length > this.sizePushed) {
                        this.imagesUploader.next(
                            new WebSocketMessage(WebSocketMessage.RECEIVE_IMAGE,
                                { id: id, type: image.extension, body: image.body.substr(this.sizePushed, block) }));
                    } else {
                        this.imagesUploader.next(
                            new WebSocketMessage(WebSocketMessage.COMPLETED_PUSH, id)
                        );
                    }
                    break;
                case WebSocketMessage.IMAGE_URL:
                    this.setUrl(msg.message);
            }
        });
    }

}
