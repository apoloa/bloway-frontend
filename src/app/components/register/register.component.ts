import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BlowayValidators } from '../../directives/validators';
import { UsersService } from '../../services/users.service';
import { APIError } from '../../models/apiError';
import { SessionService } from '../../services/session.service';

@Component({
    selector: 'register-page',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})

export class RegisterComponent {
    user: User;
    userForm: FormGroup = this.fb.group({
        username: ['', Validators.required],
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        email: ['', Validators.compose([Validators.required, Validators.email])],
        password: ['', Validators.compose([Validators.required, BlowayValidators.strongPassword])]
    });

    constructor(public fb: FormBuilder,
                private _usersService: UsersService,
                private _sessionService: SessionService,
                private _router: Router) {
        this.user = new User();
    }

    getErrorMessage(form: string): string {
        if (this.userForm.controls[form]) {
            if (this.userForm.controls[form].hasError('required')) {
                return 'Required';
            }
            if (this.userForm.controls[form].hasError('email')) {
                return 'Invalid email';
            }
            if (this.userForm.controls[form].hasError('notUnique')) {
                return 'Already Exists';
            }
            if (this.userForm.controls[form].hasError('strongPassword')) {
                return 'Passwords must need 1 capital letter and number and 8 characters with minimum';
            }
        }
        return null;
    }

    submitRegister(): void {
        this._usersService.registerUser(this.user).subscribe(
            tokens => {
                this._sessionService.setTokens(tokens);
                this._router.navigate((['']));
            },
            error => this.setErrorBackendMessage(error)
        );
    }

    private setErrorBackendMessage(error: APIError) {
        switch (error.id) {
            case APIError.UsernameAlreadyRegistered:
                this.userForm.controls['username'].setErrors({
                    'notUnique': true
                });
                break;
            case APIError.EmailAlreadyRegistered:
                this.userForm.controls['email'].setErrors({
                    'notUnique': true
                });
        }
    }
}
