import { Component, EventEmitter, Input, Output } from '@angular/core';
import { RowId } from '../../models/rowId';

@Component({
    selector: 'create-body',
    templateUrl: './create-body.component.html',
    styleUrls: ['./create-body.component.css']
})

export class CreateBodyComponent {
    body: string = '';
    @Input() row: number;
    @Output() completed: EventEmitter<RowId> = new EventEmitter();

    change(event: any): void {
        console.log(event);
        let rowId = new RowId(this.row, this.body, null, null, null, null);
        this.completed.next(rowId);
    }
}
