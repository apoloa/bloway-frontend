import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'login-register-modal',
    templateUrl: './login-register-modal.component.html',
    styleUrls: ['./login-register-modal.component.css']
})

export class LoginRegisterModalComponent {

    private modalRef: NgbModalRef = null;

    constructor(private _modalService: NgbModal, private _router: Router) {
    }

    showLogin(): void {
        this.closeModal();
        this._router.navigate(['/login']);
    }

    showRegister(): void {
        this.closeModal();
        this._router.navigate(['./register']);
    }

    closeModal(): void {
        if (this.modalRef) {
            this.modalRef.close();
        }
    }

    open(content: any) {
        this.modalRef = this._modalService.open(content);
    }
}
