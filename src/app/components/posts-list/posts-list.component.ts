import { Component, Input } from '@angular/core';
import { PostUser } from '../../models/postUser';
import { Router } from '@angular/router';

@Component({
    selector: 'posts-list',
    templateUrl: './posts-list.component.html'
})

export class PostListComponent {
    @Input() posts: PostUser[];

    constructor(private _router: Router) {
    }

    showDetailPost(id: string): void {
        this._router.navigate(['/post', id]);
    }
}
