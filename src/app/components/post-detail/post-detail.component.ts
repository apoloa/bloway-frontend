import { Component, OnInit } from '@angular/core';
import { PostUser } from '../../models/postUser';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'post-detail',
    templateUrl: './post-detail.component.html',
    styleUrls: ['./post-detail.component.css']
})

export class PostDetailComponent implements OnInit {
    post: PostUser;

    constructor(private _activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {
        this._activatedRoute.data.forEach((data : {post: PostUser}) => {
            this.post = data.post;
        });
        window.scrollTo(0,0);
    }

}
