import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Login } from '../../models/login';
import { BlowayValidators } from '../../directives/validators';
import { UsersService } from '../../services/users.service';
import { SessionService } from '../../services/session.service';
import { Router } from '@angular/router';
import { APIError } from '../../models/apiError';

@Component({
    selector: 'login-page',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent {

    login: Login;
    error: string = null;
    loginForm: FormGroup = this.fb.group({
        email: ['', Validators.compose([Validators.required, Validators.email])],
        password: ['', Validators.compose([Validators.required, BlowayValidators.strongPassword])]
    });

    constructor(public fb: FormBuilder,
                private _usersService: UsersService,
                private _sessionService: SessionService,
                private _router: Router) {
        this.login = new Login();
    }

    getErrorMessage(form: string): string {
        if (this.loginForm.controls[form]) {
            if (this.loginForm.controls[form].hasError('required')) {
                return 'Required';
            }
            if (this.loginForm.controls[form].hasError('email')) {
                return 'Invalid email';
            }
            if (this.loginForm.controls[form].hasError('strongPassword')) {
                return 'Passwords must need 1 capital letter and number and 8 characters with minimum';
            }
        }
        return null;
    }

    setErrorBackendMessage(error: APIError) {
        if (error.id === APIError.InvalidLoginParameters) {
            this.error = 'Invalid Login';
        }
    }

    submitLogin(form: FormGroup): void {
        this._usersService.loginUser(this.login).subscribe(
            tokens => {
                this._sessionService.setTokens(tokens);
                this._router.navigate((['']));
            },
            error => this.setErrorBackendMessage(error)
        );
    }
}
