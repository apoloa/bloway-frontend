import { Component, ElementRef, EventEmitter, Output } from '@angular/core';
import { Image } from '../../models/image';
@Component({
    selector: 'input-reader',
    templateUrl: './input-reader.component.html',
    styleUrls: ['./input-reader.component.css']
})

export class InputFileReaderComponent {

    @Output() completed: EventEmitter<Image> = new EventEmitter();

    private file: File;

    private static base64ToArrayBuffer(buffer) {
        let binary = '';
        let bytes = new Uint8Array( buffer );
        let len = bytes.byteLength;
        for (let i = 0; i < len; i++) {
            binary += String.fromCharCode( bytes[ i ] );
        }
        return window.btoa( binary );
    }

    valid(): boolean {
        return this.file === null;
    }

    submit() {
        let self = this;
        if (this.file) {
            let myReader: FileReader = new FileReader();
            myReader.readAsArrayBuffer(this.file);
            myReader.onloadend = function (e) {
                let resultSet = InputFileReaderComponent.base64ToArrayBuffer(myReader.result);
                let image = new Image(self.file.name.split('.')[1], resultSet);
                self.completed.next(image);
            };
        }
    }

    changeListener($event: any) {
        this.file = $event.target.files[0];
    }


}
