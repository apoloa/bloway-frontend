import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PostUser } from '../../models/postUser';
import { StorageService } from '../../services/storage.service';

@Component({
    selector: 'post-viewer',
    templateUrl: './post-viewer.component.html',
    styleUrls: ['./post-viewer.component.css']
})

export class PostViewerComponent {
    @Input() post: PostUser;
    @Output() showDetailPost: EventEmitter<string> = new EventEmitter();

    constructor(private storage: StorageService) {

    }

    showDetail(): void {
        this.storage.pushElement(this.post);
        this.showDetailPost.emit(this.post.post.id);
    }

}
