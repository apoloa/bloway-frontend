import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { CreateBodyComponent } from '../create-body/create-body.component';
import { PushImageComponent } from '../push-image/push-image.component';
import { Row } from '../../models/row';
import { Post } from '../../models/post';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RowId } from '../../models/rowId';
import { PostsService } from '../../services/post.service';
import { Router } from '@angular/router';

@Component({
    selector: 'create-post',
    templateUrl: './create-post.component.html',
    styleUrls: ['./create-post.component.css']
})

export class CreatePostComponent {

    rows: Row[] = [];
    id: number = 0;
    post: Post;
    postForm: FormGroup = this.fb.group({
        title: ['', Validators.required],
        summary: ['', Validators.required]
    });

    constructor(public fb: FormBuilder,
                private componentFactoryResolver: ComponentFactoryResolver,
                private viewContainerRef: ViewContainerRef,
                private _postService: PostsService,
                private _router: Router) {
        this.post = new Post();
    }

    getErrorMessage(form: string): string {
        if (this.postForm.controls[form]) {
            if (this.postForm.controls[form].hasError('required')) {
                return 'Required';
            }
        }
        return null;
    }

    addBody(): void {
        let factory = this.componentFactoryResolver.resolveComponentFactory(CreateBodyComponent);
        let ref = this.viewContainerRef.createComponent(factory);
        ref.changeDetectorRef.detectChanges();
        ref.instance.row = this.id;
        this.rows.push(new Row(null, null, null, null, null));
        ref.instance.completed.subscribe(RowId => {
            console.log('Updated Body');
            this.rows[RowId.id] = <Row> RowId;
            console.log(this.rows);
        });
        this.id++;
    }

    addImage(): void {
        let factory = this.componentFactoryResolver.resolveComponentFactory(PushImageComponent);
        let ref = this.viewContainerRef.createComponent(factory);
        ref.changeDetectorRef.detectChanges();
        ref.instance.row = this.id;
        ref.instance.completed.subscribe(RowId => {
            console.log('Updated Image');
            this.rows[RowId.id] = <Row> RowId;
            console.log(this.rows);
        });
        this.id++;
    }

    pushedImage(event: RowId):void {
        console.log(event);
        this.post.urlImage = event.url;
    }

    submitPost(): void {
        this.post.rows = this.rows;
        this._postService.createPost(this.post).subscribe(
            tokens => {
                this._router.navigate([`/posts/category/${this.post.category}`]);
            },
            error => console.log(error)
        );

    }
}

