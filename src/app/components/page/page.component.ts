import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostPage } from '../../models/postPage';

@Component({
    templateUrl: './page.component.html'
})
export class PageComponent implements OnInit {
    pagePost: PostPage;

    constructor(private _activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {
        this._activatedRoute.data.forEach((data : {page: PostPage}) => {
            this.pagePost = data.page;
        });
        window.scrollTo(0,0);
    }

}
