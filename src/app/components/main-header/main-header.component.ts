import { Component } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { Router } from '@angular/router';

@Component({
    selector: 'main-header',
    templateUrl: './main-header.component.html',
    styleUrls: ['./main-header.component.css']
})

export class MainHeaderComponent {
    constructor(private _sessionService: SessionService,
                private _router: Router) {
    }

    checkRegistered(): boolean {
        return this._sessionService.hasSession();
    }

    goToNewPost(): void {
        this._router.navigate(['/post/create']);
    }
}
