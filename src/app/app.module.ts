import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { CookieService } from 'ng2-cookies';

// Import own elements
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { CategoriesHeaderComponent } from './components/categories-header/categories-header.component';
import { MainHeaderComponent } from './components/main-header/main-header.component';
import { LoginRegisterModalComponent } from './components/login-register-modal/login-register-modal.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { APIProvider, APIWebSocketProvider } from './services/settings.service';
import { UsersService } from './services/users.service';
import { SessionService } from './services/session.service';
import { WebSocketService } from './services/websocket.service';
import { ImagesService } from './services/images.service';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { PushImageComponent } from './components/push-image/push-image.component';
import { InputFileReaderComponent } from './components/input-reader/input-reader.component';
import { PostViewerComponent } from './components/post-viewer/post-viewer.component';
import { PostsService } from './services/post.service';
import { PostListComponent } from './components/posts-list/posts-list.component';
import { PostsResolve } from './services/posts-resolve.service';
import { PageComponent } from './components/page/page.component';
import { CreateBodyComponent } from './components/create-body/create-body.component';
import { StorageService } from './services/storage.service';
import { StorageResolve } from './services/storage-resolve.service';
import { PostDetailComponent } from './components/post-detail/post-detail.component';


@NgModule({
    imports: [
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        NgbModule.forRoot()
    ],

    declarations: [
        AppComponent,
        CategoriesHeaderComponent,
        MainHeaderComponent,
        LoginRegisterModalComponent,
        LoginComponent,
        RegisterComponent,
        CreatePostComponent,
        PushImageComponent,
        InputFileReaderComponent,
        PostViewerComponent,
        PostListComponent,
        PageComponent,
        CreateBodyComponent,
        PostDetailComponent
    ],

    providers: [
        CookieService,
        APIProvider,
        APIWebSocketProvider,
        UsersService,
        SessionService,
        WebSocketService,
        ImagesService,
        PostsService,
        PostsResolve,
        StorageService,
        StorageResolve
    ],

    bootstrap: [
        AppComponent
    ]
})

export class AppModule {
}
