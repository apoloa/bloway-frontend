import { AbstractControl, ValidationErrors } from '@angular/forms';

export class BlowayValidators {

    static strongPassword(control: AbstractControl): ValidationErrors | null {
        const strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})');
        if (strongRegex.test(control.value)) {
            return null;
        } else {
            return {
                strongPassword: true
            };
        }
    }

}
