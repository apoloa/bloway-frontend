import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PushImageComponent } from './components/push-image/push-image.component';
import { PostViewerComponent } from './components/post-viewer/post-viewer.component';
import { PostsResolve } from './services/posts-resolve.service';
import { PageComponent } from './components/page/page.component';
import { CreateBodyComponent } from './components/create-body/create-body.component';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { PostDetailComponent } from './components/post-detail/post-detail.component';
import { StorageResolve } from './services/storage-resolve.service';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'post/create',
        component: CreatePostComponent
    },
    {
        path: 'post/:id',
        component: PostDetailComponent,
        resolve: {
            post: StorageResolve
        }
    },
    {
        path: 'demo/post',
        component: PostViewerComponent
    },
    {
        path: 'demo/body',
        component: CreateBodyComponent
    },
    {
        path: 'demo/image',
        component: PushImageComponent
    },
    {
        path: 'posts/category/:categoryId',
        component: PageComponent,
        resolve: {
            page: PostsResolve
        }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
