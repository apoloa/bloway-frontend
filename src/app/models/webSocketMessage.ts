import { isBoolean, isNumber, isString } from 'util';

const websocketMessagePrefix = 'iris-websocket-message:';
const websocketMessageSeparator = ';';

const websocketStringMessageType = 0;
const websocketIntMessageType = 1;
const websocketBoolMessageType = 2;
const websocketJSONMessageType = 4;

const websocketMessagePrefixLen = websocketMessagePrefix.length;
const websocketMessageSeparatorLen = websocketMessageSeparator.length;
const websocketMessagePrefixAndSepIdx = websocketMessagePrefixLen + websocketMessageSeparatorLen - 1;
const websocketMessagePrefixIdx = websocketMessagePrefixLen - 1;
const websocketMessageSeparatorIdx = websocketMessageSeparatorLen - 1;

export class WebSocketMessage {
    static GET_ID: string = 'id';
    static RECEIVE_IMAGE: string = 'image-rec';
    static COMPLETED_PUSH: string = 'completed';
    static SEND_ID_FOR_IMAGE: string = 'image-id';
    static SEND_CORRECT_READ: string = 'part-image-read';
    static IMAGE_URL: string = 'image-url';

    static decodeMessage(message: string): WebSocketMessage {
        let event = this.getWebsocketCustomEvent(message);
        let messageRet = this.getCustomMessage(event, message);
        return new WebSocketMessage(event, messageRet);
    }

    private static getWebsocketCustomEvent(websocketMessage: string): string {
        if (websocketMessage.length < websocketMessagePrefixAndSepIdx) {
            return '';
        }
        let s = websocketMessage.substring(websocketMessagePrefixAndSepIdx, websocketMessage.length);
        return s.substring(0, s.indexOf(websocketMessageSeparator));

    }

    private static getCustomMessage(event: string, websocketMessage: string): string {
        let eventIdx = websocketMessage.indexOf(event + websocketMessageSeparator);
        return websocketMessage.substring(eventIdx + event.length + websocketMessageSeparator.length + 2, websocketMessage.length);
    }

    private static createMessage(event: string, websocketMessageType: number, dataMessage: string): string {
        return websocketMessagePrefix + event + websocketMessageSeparator + String(websocketMessageType) +
            websocketMessageSeparator + dataMessage;
    }

    private static isJSON(obj: any): boolean {
        return typeof obj === 'object';
    }

    encode(): string {
        let message = '';
        let type = 0;
        if (isNumber(this.message)) {
            type = websocketIntMessageType;
            message = this.message.toString();
        } else if (isBoolean(this.message)) {
            type = websocketBoolMessageType;
            message = this.message.toString();
        } else if (isString(this.message)) {
            type = websocketStringMessageType;
            message = this.message.toString();
        } else if (WebSocketMessage.isJSON(this.message)) {
            type = websocketJSONMessageType;
            message = JSON.stringify(this.message);
        } else {
            console.log('Invalid, javascript-side should contains an empty second parameter.');
        }
        return WebSocketMessage.createMessage(this.messageId, type, message);
    }

    constructor(public messageId: string,
                public message: any) {
    }
}
