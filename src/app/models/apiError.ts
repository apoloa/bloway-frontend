export class APIError {

    static UsernameAlreadyRegistered: number = 3;
    static EmailAlreadyRegistered: number = 4;
    static InvalidLoginParameters: number = 8;

    static fromJSON(json: any): APIError {
        return new APIError(
            json.id,
            json.statusCode,
            json.message
        );
    }

    static fromString(message: any): APIError {
        return new APIError(
            -1,
            -1,
            message
        );
    }

    private constructor(public id: number,
                        public statusCode: number,
                        public message: string) {
    }

}

