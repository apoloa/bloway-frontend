import { Row } from './row';
import { isArray } from  'util';

export class Post {
    public id: string;
    public title: string;
    public summary: string;
    public urlImage: string;
    public rows: Row[];
    public blog: number;
    public likes: number;
    public createAt: number;
    public owner: number;
    public category: number;

    static fromJSON(json: any): Post {
        let rows: Row[] = [];
        if (json.hasOwnProperty('rows') && isArray(json.rows)) {
            rows = json.rows.map(row => Row.fromJSON(row));
        }
        let post = new Post();
        post.id = json.id;
        post.title = json.title;
        post.summary = json.summary;
        post.urlImage = json.urlImage;
        post.rows = rows;
        post.blog = json.blog;
        post.likes = json.likes;
        post.createAt = json.createAt;
        post.owner = json.owner;
        post.category = json.category;
        return post;
    }

    constructor() {
        this.category = 1;
    }
}
