import { User } from './user';
export class Blog {

    public user: User;

    static fromJSON(json: any): Blog {
        return new Blog(
            json.id,
            json.owner,
            json.name,
            json.urlPhoto,
            json.access,
            json.likes,
            json.createAt
        );
    }

    private constructor(public id: number,
                        public owner: number,
                        public name: string,
                        public urlPhoto: string,
                        public access: number,
                        public likes: number,
                        public createAt: number) {
    }

}
