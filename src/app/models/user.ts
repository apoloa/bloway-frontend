export class User {
    public userName: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public password: string;

    static fromJSON(json: any): User {
        let user = new User();
        user.userName = json.userName;
        user.firstName = json.firstName;
        user.lastName = json.lastName;
        return user;
    }
}
