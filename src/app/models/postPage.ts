import { isArray } from 'util';
import { PostUser } from './postUser';

export class PostPage {
    static fromJSON(json: any): PostPage {
        let post: PostUser[] = [];
        if (json.hasOwnProperty('posts') && isArray(json.posts)) {
            post = json.posts.map(row => PostUser.fromJSON(row));
        }
        return new PostPage(
            json.page,
            json.elements,
            post
        );
    }

    private constructor(public page: number,
                        public elements: number,
                        public post: PostUser[]) {
    }

}
