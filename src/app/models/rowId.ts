import { Row } from './row';
export class RowId extends Row {

    public constructor(public id: number,
                        public body: string,
                        public url: string,
                        public height: number,
                        public width: number,
                        public note: string) {
        super(body, url, height, width, note);
    }

}
