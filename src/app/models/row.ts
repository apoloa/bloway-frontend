export class Row {

    static fromJSON(json: any): Row {
        return new Row(
            json.body,
            json.url,
            json.height,
            json.width,
            json.note
        );
    }

    public constructor(public body: string,
                        public url: string,
                        public height: number,
                        public width: number,
                        public note: string) {
    }

}
