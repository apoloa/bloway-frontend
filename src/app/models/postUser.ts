import { Post } from './post';
import { User } from './user';

export class PostUser {

    static fromJSON(json: any): PostUser {
        return new PostUser(
            Post.fromJSON(json.post),
            User.fromJSON(json.user)
        );
    }

    private constructor(public post: Post,
                        public user: User) {
    }

}
