export class Tokens {

    static fromJSON(json: any): Tokens {
        return new Tokens(
            json.accessToken,
            json.refreshToken,
            json.timeRefresh
        );
    }

    private constructor(public accessToken: string,
                        public refreshToken: string,
                        public timeRefresh: number) {
    }

}
