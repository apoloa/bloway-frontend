import { CookieService } from 'ng2-cookies';
import { Injectable } from '@angular/core';
import { Tokens } from '../models/tokens';

const ACCESS_TOKEN_KEY = 'accessToken';
const REFRESH_TOKEN_KEY = 'refreshToken';

@Injectable()
export class SessionService {

    constructor(private _storage: CookieService) {
    }

    hasSession(): boolean {
        return this.checkIfExists(ACCESS_TOKEN_KEY);
    }

    getAccessToken(): string | null {
        if (this.checkIfExists(ACCESS_TOKEN_KEY)) {
            return this._storage.get(ACCESS_TOKEN_KEY);
        }
        return null;
    }

    getRefreshToken(): string {
        if (this.checkIfExists(REFRESH_TOKEN_KEY)) {
            return this._storage.get(REFRESH_TOKEN_KEY);
        }
        return null;
    }

    setTokens(tokens: Tokens) {
        this._storage.set(ACCESS_TOKEN_KEY, tokens.accessToken);
        this._storage.set(REFRESH_TOKEN_KEY, tokens.refreshToken);
    }

    private checkIfExists(key: string): boolean {
        return this._storage.check(key);
    }
}
