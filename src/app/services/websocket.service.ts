import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { WebSocketMessage } from '../models/webSocketMessage';
import 'rxjs/add/operator/map';
@Injectable()
export class WebSocketService {
    private subject: Subject<WebSocketMessage>;


    public connect(url: string): Subject<WebSocketMessage> {
        if (!this.subject) {
            this.subject = this.create(url);
        }
        return this.subject;
    }

    public create(url: string): Subject<WebSocketMessage> {
        let ws = new WebSocket(url);

        let observable = Observable.create(
            (obs: Observer<WebSocketMessage>) => {
                ws.onmessage = obs.next.bind(obs);
                ws.onerror = obs.error.bind(obs);
                ws.onclose = obs.complete.bind(obs);

                return ws.close.bind(ws);
            });

        let observer = {
            next: (data: WebSocketMessage) => {
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(data.encode());
                }
            }
        };

        return Subject.create(observer, observable)
            .map((message: MessageEvent): WebSocketMessage => {
                let messageSocket = <string>message.data;
                return WebSocketMessage.decodeMessage(messageSocket);
            });

    }


}
