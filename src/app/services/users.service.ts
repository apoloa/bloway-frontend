import { Inject, Injectable } from '@angular/core';
import { Tokens } from '../models/tokens';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/user';
import { Http, Response } from '@angular/http';
import { API } from './settings.service';
import { APIError } from '../models/apiError';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Login } from '../models/login';

@Injectable()
export class UsersService {

    constructor(private _http: Http,
                @Inject(API) private _api: string) {

    }

    registerUser(user: User): Observable<Tokens> {
        return this._http
            .post(`${this._api}/user`, user)
            .map((response: Response) => {
                const json = response.json();
                return Tokens.fromJSON(json);
            })
            .catch(this.handleError);
    }

    loginUser(login: Login): Observable<Tokens> {
        return this._http
            .post(`${this._api}/user/login`, login)
            .map((response: Response) => {
                const json = response.json();
                return Tokens.fromJSON(json);
            })
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        let errorMessage: APIError;
        if (error instanceof Response) {
            const body = error.json() || '';
            errorMessage = APIError.fromJSON(body);
        } else {
            errorMessage = APIError.fromJSON(error.message ? error.message : error.toString());
        }
        console.error(errorMessage);
        return Observable.throw(errorMessage);
    }
}
