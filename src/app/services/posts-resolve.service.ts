import { Injectable } from '@angular/core';
import { PostPage } from '../models/postPage';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { PostsService } from './post.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PostsResolve implements Resolve<PostPage> {

    constructor(private _postService: PostsService) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<PostPage> {
        const categoryId = route.params['categoryId'];
        if (categoryId) {
            return this._postService.getPostByCategory(categoryId);
        }
        return this._postService.getPostByCategory(10);
    }

}
