import { Injectable } from '@angular/core';
import { PostUser } from '../models/postUser';


@Injectable()
export class StorageService {

    private elements: PostUser[] = [];

    pushElement(post: PostUser): void {
        for (let element of this.elements) {
            if (element.post.id === post.post.id) {
                return;
            }
        }
        this.elements.push(post);
    }

    getElement(id: string): Promise<PostUser> {
        for (let element of this.elements) {
            if (element.post.id === id) {
                return Promise.resolve(element);
            }
        }
        return Promise.reject('Not Found');
    }


}
