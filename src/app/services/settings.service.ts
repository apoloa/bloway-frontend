import { InjectionToken } from '@angular/core';
export const API: InjectionToken<string> = new InjectionToken('API');
export const APIProvider = {
    provide: API,
    useValue: 'http://localhost:8080/v1'
};
export const APIWebSocket: InjectionToken<string> = new InjectionToken('APIWebSocket');
export const APIWebSocketProvider = {
    provide: APIWebSocket,
    useValue: 'ws://localhost:8080/ws'
};
