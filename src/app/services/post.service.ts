import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Headers, Http, Response } from '@angular/http';
import { API } from './settings.service';
import { APIError } from '../models/apiError';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { PostPage } from '../models/postPage';
import { SessionService } from './session.service';
import { Post } from '../models/post';

@Injectable()
export class PostsService {

    constructor(private _http: Http,
                @Inject(API) private _api: string,
                private _session: SessionService) {

    }

    getPostByCategory(id: number): Observable<PostPage> {
        return this._http
            .get(`${this._api}/posts/category/${id}`)
            .map((response: Response) => {
                const json = response.json();
                return PostPage.fromJSON(json);
            })
            .catch(this.handleError);
    }

    createPost(post: Post): Observable<Post> {
        let headers = new Headers();
        headers.append('x_access_token', this._session.getAccessToken());
        return this._http.post(`${this._api}/post`, post, {headers: headers})
            .map((response: Response) => {
                const json = response.json();
                return Post.fromJSON(json);
            })
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        let errorMessage: APIError;
        if (error instanceof Response) {
            const body = error.json() || '';
            errorMessage = APIError.fromJSON(body);
        } else {
            errorMessage = APIError.fromJSON(error.message ? error.message : error.toString());
        }
        console.error(errorMessage);
        return Observable.throw(errorMessage);
    }
}
