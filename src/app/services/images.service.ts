import { Inject, Injectable } from '@angular/core';
import { WebSocketService } from './websocket.service';
import { APIWebSocket } from './settings.service';
import { Subject } from 'rxjs/Subject';
import { WebSocketMessage } from '../models/webSocketMessage';

@Injectable()
export class ImagesService {


    constructor(private _ws: WebSocketService,
                @Inject(APIWebSocket) private _api: string) {
    }

    createImage(): Subject<WebSocketMessage> {
        return this._ws.create(this._api);
    }
}
