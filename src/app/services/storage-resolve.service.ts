import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { PostUser } from '../models/postUser';
import { StorageService } from './storage.service';

@Injectable()
export class StorageResolve implements Resolve<PostUser> {

    constructor(private _storageService: StorageService) {
    }

    resolve(route: ActivatedRouteSnapshot): Promise<PostUser> {
        const id = route.params['id'];
        if (id) {
            return this._storageService.getElement(id);
        }
        return Promise.reject('Not found');
    }

}
